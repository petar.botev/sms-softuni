﻿using SMS.ViewModels;
using System.Collections.Generic;

namespace SMS.HelperMethods
{
    public interface IValidateRegisterForm
    {
        ICollection<string> Validate(RegisterFormModel model);
    }
}