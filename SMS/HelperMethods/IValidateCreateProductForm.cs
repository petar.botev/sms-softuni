﻿using SMS.ViewModels;
using System.Collections.Generic;

namespace SMS.HelperMethods
{
    public interface IValidateCreateProductForm
    {
        ICollection<string> Validate(CreateProductFormModel model);
    }
}