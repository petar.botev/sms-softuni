﻿using SMS.ViewModels;
using System.Collections.Generic;

namespace SMS.HelperMethods
{
    public class ValidateCreateProductForm : IValidateCreateProductForm
    {
        public ICollection<string> Validate(CreateProductFormModel model)
        {
            ICollection<string> errors = new List<string>();

            //check Username
            if (string.IsNullOrWhiteSpace(model.Name)
                || model.Name.Length < 4
                || model.Name.Length > 20)
            {
                errors.Add("Username must be between 5 and 20 symbols!");
            }

            //check Price
            if (model.Price < 0.05M || model.Price > 1000M || model.Price == 0)
            {
                errors.Add("Price must be between 0,05 and 1000");
            }

            return errors;
        }
    }
}
