﻿using SMS.ViewModels;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace SMS.HelperMethods
{
    public class ValidateRegisterForm : IValidateRegisterForm
    {
        public ICollection<string> Validate(RegisterFormModel model)
        {
            ICollection<string> errors = new List<string>();

            //check Username
            if (string.IsNullOrWhiteSpace(model.Username)
                || model.Username.Length < 5
                || model.Username.Length > 20)
            {
                errors.Add("Username must be between 5 and 20 symbols!");
            }

            //check Email
            if (model.Email == null
                || !Regex.IsMatch(model.Email, @"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$"))
            {
                errors.Add("Email is not valid!");
            }

            //check password
            if (string.IsNullOrWhiteSpace(model.Password)
                || model.Password.Length < 6
                || model.Password.Length > 20)
            {
                errors.Add("Password must be between 5 and 20 symbols!");
            }

            //check ConfirmPassword
            if (string.IsNullOrWhiteSpace(model.ConfirmPassword)
                || model.ConfirmPassword != model.Password)
            {
                errors.Add("Password does not match!");
            }

            return errors;
        }
    }
}
