﻿namespace SMS.HelperMethods
{
    public interface IPasswordHasher
    {
        string HashPassword(string password);
    }
}