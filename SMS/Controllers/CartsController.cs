﻿using MyWebServer.Controllers;
using MyWebServer.Http;
using SMS.Data;
using SMS.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace SMS.Controllers
{
    [Authorize]
    public class CartsController : Controller
    {
        private readonly SMSDbContext ctx;

        public CartsController(SMSDbContext ctx)
        {
            this.ctx = ctx;
        }

       
        public HttpResponse Details()
        {
            var cart = ctx
                .Carts
                .Where(x => x.UserId == User.Id)
                .FirstOrDefault();

            if (cart == null)
            {
                return View(new List<DetailsViewModel>());
            }

            var products = ctx
                .Products
                .Where(p => p.CartId == cart.Id)
                .Select(p => new DetailsViewModel
                {
                    Id = p.Id,
                    Name = p.Name,
                    Price = p.Price,
                })
                .ToArray();

            return View(products);
        }

        public HttpResponse AddProduct(string productId)
        {
            var product = ctx
                .Products
                .Where(p => p.Id == productId)
                .FirstOrDefault();

            var productModel = new AddProductViewModel
            {
                Id = product.Id,
                Name = product.Name,
                Price = product.Price,
            };

            return View("/Products/Add", productModel);
        }

        public HttpResponse BuyAllProducts()
        {
            var cart = ctx
                .Carts
                .Where(c => c.UserId == User.Id)
                .FirstOrDefault();

            var cartsProducts = ctx
                .Products
                .Where(p => p.CartId == cart.Id)
                .ToArray();

            foreach (var product in cartsProducts)
            {
                product.CartId = null;
            }

            ctx.Carts.Update(cart);
            ctx.SaveChanges();

            return Redirect("/Home/IndexLoggedIn");
        }
    }
}
