﻿using MyWebServer.Controllers;
using MyWebServer.Http;
using SMS.Data;
using SMS.HelperMethods;
using SMS.Models;
using SMS.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SMS.Controllers
{
    public class ProductsController : Controller
    {
        private readonly IValidateCreateProductForm validation;
        private readonly SMSDbContext ctx;

        public ProductsController(IValidateCreateProductForm validation, SMSDbContext ctx)
        {
            this.validation = validation;
            this.ctx = ctx;
        }

        [HttpGet]
        public HttpResponse Create()
        {
            return this.View();
        }

        [HttpPost]
        public HttpResponse Create(CreateProductFormModel model)
        {
            var validationError = validation.Validate(model);

            if (validationError.Count != 0)
            {
                return Error(validationError);
            }

            var product = new Product
            {
                Id = Guid.NewGuid().ToString(),
                Name = model.Name,
                Price = model.Price,
            };

            ctx.Add(product);
            ctx.SaveChanges();

            return this.Redirect("/Home/IndexLoggedIn");
        }

        [HttpPost]
        public HttpResponse Add(AddProductFormModel model)
        {
            var user = ctx
                .Users
                .Where(u => u.Id == User.Id)
                .FirstOrDefault();

            var product = new Product
            {
                Id = model.Id,
                Name = model.Name,
                Price = model.Price,
                CartId = user.CartId,
            };

            ctx.Products.Update(product);
            ctx.SaveChanges();

            return Redirect("/Carts/Details");
        }
    }
}
