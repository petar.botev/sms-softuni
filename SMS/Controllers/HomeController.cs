﻿namespace SMS.Controllers
{
    using MyWebServer.Controllers;
    using MyWebServer.Http;
    using SMS.Data;
    using SMS.HelperMethods;
    using SMS.ViewModels;
    using System.Linq;

    public class HomeController : Controller
    {
        private readonly IValidateRegisterForm validateRegisterForm;
        private readonly SMSDbContext ctx;
        private readonly IPasswordHasher passwordHasher;

        public HomeController(SMSDbContext ctx, IValidateRegisterForm validateRegisterForm, IPasswordHasher passwordHasher)
        {
            this.ctx = ctx;
            this.validateRegisterForm = validateRegisterForm;
            this.passwordHasher = passwordHasher;
        }

        public HttpResponse Index()
        {
            return this.View();
        }

        [Authorize]
        public HttpResponse IndexLoggedIn()
        {
            var user = this.ctx
                .Users
                .FirstOrDefault(u => u.Id == User.Id);

            var products = this.ctx
                .Products
                .ToArray();

            var userModel = new LoggedIndexViewModel
            {
                Username = user.Username,
                Password = user.Password,
                Email = user.Email,
                Products = products
            };

            return this.View(userModel);
        }
    }
}