﻿using MyWebServer.Controllers;
using MyWebServer.Http;
using SMS.Data;
using SMS.HelperMethods;
using SMS.Models;
using SMS.ViewModels;
using System;
using System.Linq;

namespace SMS.Controllers
{
    public class UsersController : Controller
    {
        private readonly IValidateRegisterForm validateRegisterForm;
        private readonly SMSDbContext ctx;
        private readonly IPasswordHasher passwordHasher;

        public UsersController(IValidateRegisterForm validateRegisterForm, 
                    SMSDbContext ctx, 
                    IPasswordHasher passwordHasher)
        {
            this.validateRegisterForm = validateRegisterForm;
            this.ctx = ctx;
            this.passwordHasher = passwordHasher;
        }

        [NotAuthorize]
        [HttpGet]
        public HttpResponse Login()
        {
            return View();
        }

        [NotAuthorize]
        [HttpPost]
        public HttpResponse Login(LoginFormModel model)
        {
            string hashedPassword = passwordHasher.HashPassword(model.Password);

            string userId = this.ctx
                .Users
                .Where(u => u.Password == hashedPassword && u.Username == model.UserName)
                .Select(x => x.Id)
                .FirstOrDefault();

            if (userId == null)
            {
                return Error("User or Password is not valid.");
            }
            
            SignIn(userId); 
            return Redirect("/Home/IndexLoggedIn");
        }

        [NotAuthorize]
        [HttpGet]
        public HttpResponse Register()
        {
            return View();
        }

        [NotAuthorize]
        [HttpPost]
        public HttpResponse Register(RegisterFormModel model)
        {
            var validationErrors = validateRegisterForm.Validate(model);

            if (validationErrors.Count != 0)
            {
                return Error(validationErrors);
            }

            //check if user exists
            var isUserExists = ctx
                .Users
                .Any(u => u.Username == model.Username || u.Email == model.Email);

            if (isUserExists)
            {
                return Error("User already exists.");
            }

            string hashedPassword = passwordHasher.HashPassword(model.Password);

            var user = new User()
            {
                Id = Guid.NewGuid().ToString(),
                Username = model.Username,
                Email = model.Email,
                Password = hashedPassword,
                CartId = Guid.NewGuid().ToString(),
            };

            var cart = new Cart()
            {
                Id = user.CartId,
                UserId = user.Id
            };

            ctx.Add(cart);
            ctx.Add(user);
            ctx.SaveChanges();

            return Redirect("Login");
        }

        [Authorize]
        public HttpResponse Logout()
        {
            this.SignOut();

            return Redirect("/");
        }
    }
}
