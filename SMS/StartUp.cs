﻿namespace SMS
{
    using System.Threading.Tasks;

    using MyWebServer;
    using MyWebServer.Controllers;
    using MyWebServer.Results.Views;
    using SMS.HelperMethods;

    public class StartUp
    {
        public static async Task Main()
            => await HttpServer
                .WithRoutes(routes => routes
                    .MapStaticFiles()
                    .MapControllers())
                .WithServices(services => services
                    .Add<IViewEngine, CompilationViewEngine>()
                    .Add<IValidateRegisterForm, ValidateRegisterForm>()
                    .Add<IPasswordHasher, PasswordHasher>()
                    .Add<IValidateCreateProductForm, ValidateCreateProductForm>())
                .Start();
    }
}