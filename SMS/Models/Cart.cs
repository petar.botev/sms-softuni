﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SMS.Models
{
    public class Cart
    {
        public Cart()
        {
            this.Products = new HashSet<Product>();
        }

        public string Id { get; set; }

        [Required]
        public User User { get; set; }

        [ForeignKey(nameof(User))]
        public string UserId { get; set; }

        public ICollection<Product> Products { get; set; }
    }
}
