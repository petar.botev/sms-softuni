﻿using System.ComponentModel.DataAnnotations;

namespace SMS.Models
{
    public class Product
    {
        public string Id { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 4)]
        public string Name { get; set; }

        [Range(0.05, 1000)]
        public decimal Price { get; set; }

        public Cart Cart { get; set; }
        public string CartId { get; set; }
    }
}
