﻿namespace SMS.ViewModels
{
    public class LoginFormModel
    {
        public string Id { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }
    }
}
