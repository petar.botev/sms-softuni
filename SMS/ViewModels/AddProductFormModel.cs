﻿namespace SMS.ViewModels
{
    public class AddProductFormModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}
