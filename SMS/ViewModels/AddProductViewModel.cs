﻿namespace SMS.ViewModels
{
    public class AddProductViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}
