﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SMS.ViewModels
{
    public class DetailsViewModel
    {
        public string Id { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 4)]
        public string Name { get; set; }

        [Range(0.05, 1000)]
        public decimal Price { get; set; }
    }
}
